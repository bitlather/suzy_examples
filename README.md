SASS
====
Install:
    sudo apt-get install ruby1.9.1-dev
    sudo gem install --version '~>0.9' rb-inotify
    sudo gem install sass

Your SASS files will end in extension .scss.

Run:
    sass --watch scss/:public/

When you save files in directory `scss`, they will be compiled to directory `public`.

To learn: http://sass-lang.com/guide

Example
-------

public/index.html:

    <LINK rel="stylesheet" href="one.css">
    <h1>Header 1</h1>
    <h2>Header 2</h2>
    <h3>Header 3</h3>
    Hi world

scss/one.scss:

    $font-stack:    Helvetica, sans-serif;
    $primary-color: #0aa;

    body {
      font: 100% $font-stack;
      color: $primary-color;
    }

    @import 'davepartial';

scss/davepartial.scss:

    h1 {
      font: 400%;
      color: green;
    }

    h2 {
      @extend h1;
      font: 200%;
    }

    h3{
      font: 10px+5px;
    }


SUSY
====
After install sass...

    gem install susy

Create project...

    compass create <project name> -r susy -u susy


To compile changes as you write scss files, CD into project and...

    compass watch

Note that config.rb can set where you write output to.

To learn:

    http://susy.oddbird.net/demos/magic/

    http://net.tutsplus.com/tutorials/html-css-techniques/responsive-grids-with-susy/